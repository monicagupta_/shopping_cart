var fruits=[
        {
            "name":"mango",
            "category":"fruit",
            "price":"80",
            "quantity":"0",
        },
        {
            "name":"banana",
            "category":"fruit",
            "price":"90",
            "quantity":"0",
        },
        {
            "name":"orange",
            "category":"fruit",
            "price":"60",
            "quantity":"0",
        },
        {
            "name":"apple",
            "category":"fruit",
            "price":"100",
            "quantity":"0",
        },
        {
            
            "name":"apricot",
            "category":"fruit",
            "price":"50",
            "quantity":"0",
        },
        {
            "name":"blackberries",
            "category":"fruit",
            "price":"75",
            "quantity":"0",
        },
        {
            "name":"grapes",
            "category":"fruit",
            "price":"70",
            "quantity":"0",
        },
        {   "name":"cherry",
            "category":"fruit",
            "price":"85",
            "quantity":"0",
        },
        {
            "name":"guava",
            "category":"fruit",
            "price":"55",
            "quantity":"0",
        },
        {
            "name":"stawberry",
            "category":"fruit",
            "price":"80",
            "quantity":"0",
        }
    ]
var vegetables=
    [
        {
            "name":"beans",
            "category":"vegetable",
            "price":"90",
            "quantity":"0",
        },
        {
            "name":"pumpkins",
            "category":"vegetable",
            "price":"80",
            "quantity":"0",
        },
        {
            "name":"potato",
            "category":"vegetable",
            "price":"60",
            "quantity":"0",
        },
        {
            "name":"tomato",
            "category":"vegetable",
            "price":"95",
            "quantity":"0",
        },
        {
            "name":"onion",
            "category":"vegetable",
            "price":"50",
            "quantity":"0",
        },
        {
            "name":"capsicum",
            "category":"vegetable",
            "price":"75",
            "quantity":"0",
        },
        {
            "name":"spinach",
            "category":"vegetable",
            "price":"70",
            "quantity":"0",
        },
        {
            "name":"mushrooms",
            "category":"vegetable",
            "price":"85",
            "quantity":"0",
        },
        {
            "name":"celery",
            "category":"vegetable",
            "price":"55",
            "quantity":"0",
        },
        {
            "name":"cabbage",
            "category":"vegetable",
            "price":"80",
            "quantity":"0",
        }
    ]

var biscuits=
    [
        {
            "name":"parle G",
            "category":"biscuit",
            "price":"80",
            "quantity":"0",
        },
        {
            "name":"oreo",
            "category":"biscuit",
            "price":"90",
            "quantity":"0",
        },
        {
            "name":"krackjack",
            "category":"biscuit",
            "price":"60",
            "quantity":"0",
        },
        {
            "name":"hide & Seek",
            "category":"biscuit",
            "price":"100",
            "quantity":"0",
        },
        {
            "name":"kreams",
            "category":"biscuit",
            "price":"50",
            "quantity":"0",
        },
        {
            "name":"festo",
            "category":"biscuit",
            "price":"75",
            "quantity":"0",
        },
        {
            "name":"nimkin",
            "category":"biscuit",
            "price":"70",
            "quantity":"0",
        },
        {
            "name":"magix",
            "category":"biscuit",
            "price":"85",
            "quantity":"0",
        },
        {
            "name":"happy happy",
            "category":"biscuit",
            "price":"55",
            "quantity":"0",
        },
        {
            "name":"top",
            "category":"biscuit",
            "price":"80",
            "quantity":"0",
        }
    ]
var dairy=
    [
        {
            "name":"butter",
            "category":"dairy",
            "price":"80",
            "quantity":"0",
        },
        {
            "name":"milk",
            "category":"dairy",
            "price":"90",
            "quantity":"0",
        },
        {
            "name":"cheese",
            "category":"dairy",
            "price":"60",
            "quantity":"0",
        },
        {
            "name":"ice cream",
            "category":"dairy",
            "price":"100",
            "quantity":"0",
        },
        {
            "name":"cream",
            "category":"dairy",
            "price":"50",
            "quantity":"0",
        },
        {
            "name":"curd",
            "category":"dairy",
            "price":"75",
            "quantity":"0",
        },
        {
            "name":"ghee",
            "category":"dairy",
            "price":"70",
            "quantity":"0",
        },
        {
            "name":"kulfi",
            "category":"dairy",
            "price":"85",
            "quantity":"0",
        },
        {
            "name":"khoa",
            "category":"dairy",
            "price":"55",
            "quantity":"0",
        },
        {
            "name":"lassi",
            "category":"dairy",
            "price":"80",
            "quantity":"0",
        },
    ]


/*  CreateTable(cart);   
function CreateTable(cart) 
{ var i;
      
  var html="<table id='head'><tr><th>Name </th><th> Category </th><th> Price</th><th> Quantity</th></tr>"; 
  for(i=0;i<cart.length;i++)
    {           
             

     html+="<tr><td>"+cart[i].name+"</td> <td>"+cart[i].category+"</td><td> "+cart[i].price+"</td></tr>";
           
    }
    html+="</table>"
    document.getElementById("body2").innerHTML = html;

}
*/       

function myFunction()
{
    var input=document.getElementById('myInput').value;
    var reg=new RegExp(input,"g");
    var print=" ";
    print="<table><tr id='head'><th> Name </th><th> Category </th><th> Price </th><th> Quantity </th></tr>";

    for( let i=0;i<fruits.length;i++)
    {
        //searches the string for a match against the regex
        if((fruits[i].name.match(reg))!=null)
        {  
           // console.log(fruits[i].name);
            print+="<tr><td>"+fruits[i].name+"  "+"</td><td>" +fruits[i].category+ "</td><td> " +fruits[i].price+ "</td><td><input type='number' id=quantity"+fruits[i].category+i+" placeholder='0' /></td></tr>";
        }  
        
    }
    console.log(print);
    for(let i=0;i<vegetables.length;i++)
    {
       
        if((vegetables[i].name.match(reg))!=null)
        {  
            //console.log(vegetables[i].name);
            print+="<table><tr><td>" +vegetables[i].name+"   "+"</td> <td>" +vegetables[i].category+ "</td><td>"+vegetables[i].price+"</td><td><input type='number' id=quantity"+vegetables[i].category+i+" placeholder='0' name='num'></td></tr>";
        }  
       
    }

    for( let i=0;i<biscuits.length;i++)
    {
        
        if((biscuits[i].name.match(reg))!=null)
        {  
            //console.log(biscuits[i].name);
            print+="<tr><td>" +biscuits[i].name+"   "+"</td> <td>" +biscuits[i].category+ "</td><td> " +biscuits[i].price+"</td><td><input type='number' id= quantity"+biscuits[i].category+i+" placeholder='0' name='num'></td></tr>";
        }  
        
    }

    for( let i=0;i<dairy.length;i++)
    {
        
        if((dairy[i].name.match(reg))!=null)
        {  
           // console.log(dairy[i].name);
            print+="<tr><td>" +dairy[i].name+"   "+"</td> <td>" +dairy[i].category+ "</td><td> " +dairy[i].price+"</td><td><input type='number' id= quantity"+dairy[i].category+i+" placeholder='0' name='num'></td></tr>";
        }  
       
                 
    }
    print+="</table>"
    document.getElementById("cat").innerHTML=print; 

}

function add()
{   //fetching what was entered in search bar
    var input1=document.getElementById('myInput').value.toLowerCase();
    var reg1=new RegExp(input1,"g");
    var print1="<table><tr><th>Name</th><th>Category</th><th>Price</th><th>Quantity</th></tr>";
    //for fruits
    for(var i=0;i<fruits.length;i++)
    {
        if(fruits[i].name.toLowerCase().match(reg1)!=null){
          fruits[i].quantity=document.getElementById("quantity"+fruits[i].category+i).value;
            console.log(fruits[i].quantity);
        }
        if(fruits[i].quantity>0){
            print1+="<tr><td>"+fruits[i].name+"</td><td>"+fruits[i].quantity+"</td></tr>"
        }
    }
    for(var i=0;i<vegetables.length;i++)
    {
        if(vegetables[i].name.toLowerCase().match(reg1)==null){ }else{
            vegetables[i].quantity=document.getElementById("quantity"+vegetables[i].category+i).value;
            console.log(vegetables[i].quantity);
        }
        if(vegetables[i].quantity>0){
            print1+="<tr><td>"+vegetables[i].name+"</td><td>"+vegetables[i].quantity+"</td></tr>"
        }
    }
    for(var i=0;i<biscuits.length;i++)
    {
        if(biscuits[i].name.toLowerCase().match(reg1)==null){ }else{
            biscuits[i].quantity=document.getElementById("quantity"+biscuits[i].category+i).value;
            console.log(biscuits[i].quantity);
        }
        if(biscuits[i].quantity>0){
            print1+="<tr><td>"+biscuits[i].name+"</td><td>"+biscuits[i].quantity+"</td></tr>"
        }
    }
    for(var i=0;i<dairy.length;i++)
    {
        if(dairy[i].name.toLowerCase().match(reg1)==null){ }else{
            dairy[i].quantity=document.getElementById("quantity"+dairy[i].category+i).value;
            console.log(dairy[i].quantity);
        }
        if(dairy[i].quantity>0){
            print1+="<tr><td>"+dairy[i].name+"</td><td>"+dairy[i].quantity+"</td></tr>"
        }
    }
    print1+="</table>";
    document.getElementById("body2").innerHTML=print1;
}







    /*
    for(var i=0;i<fruits.length;i++)
    {
        //searches the string for a match against the regex
        if(fruits[i].name.toLowerCase().match(reg1)!=null)
        {
           fruits[i].quantity=document.getElementById("quantity"+fruits[i].category+i ).value;
          //fruits[i].qty=document.getElementById(fruits[i].name).value;

        }
        if(fruits[i].quantity>0)
        {
            print1+="<tr><td>" +fruits[i].name+"</td> <td>" +fruits[i].category+ "</td><td> " +fruits[i].price+ "</td><td>"+fruits[i].quantity+"</td></tr>";

        }
    }

    //for vegetables
    for(var i=0;i<vegetables.length;i++)
    {
        //searches the string for a match against the regex
        if(vegetables[i].name.toLowerCase().match(reg1)!=null)
        {
            vegetables[i].quantity=document.getElementById("quantity"+vegetables[i].category+i).value;
            //vegetables[i].qty=document.getElementById(vegetables[i].name).value;
        }
        if(vegetables[i].quantity>0)
        {
            print1+="<tr><td>" +vegetables[i].name+"   "+"</td> <td>" +vegetables[i].category+ "</td><td> " +vegetables[i].price+ "</td><td>"+vegetables[i].quantity+"</td></tr>";

        }
    }
    
    //for biscuits
    for(var i=0;i<biscuits.length;i++)
    {
        //searches the string for a match against the regex
        if(biscuits[i].name.toLowerCase().match(reg1)!=null)
        {
            biscuits[i].quantity=document.getElementById("quantity"+biscuits[i].category+i).value;
        }
        if(biscuits[i].quantity>0)
        {
            print1+="<tr><td>" +biscuits[i].name+"   "+"</td> <td>" +biscuits[i].category+ "</td><td> " +biscuits[i].price+ "</td><td>"+biscuits[i].quantity+"</td></tr>";

        }
    }
    
    //for dairy
    for(var i=0;i<dairy.length;i++)
    {
        //searches the string for a match against the regex
        if(dairy[i].name.toLowerCase().match(reg1)!=null)
        {
            dairy[i].quantity=document.getElementById("quantity"+dairy[i].category+i).value;
        }
        if(dairy[i].quantity>0)
        {
            print1+="<tr><td>" +dairy[i].name+"   "+"</td> <td>" +dairy[i].category+ "</td><td> " +dairy[i].price+ "</td><td>"+dairy[i].quantity+"</td></tr>";

        }
    }
    print1+="</table>"
    document.getElementById("body2").innerHTML=print1; 
}

*/